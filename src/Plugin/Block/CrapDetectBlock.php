<?php

namespace Drupal\crap_detect\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Crap Detect' Block.
 *
 * @Block(
 *   id = "crap_detect",
 *   admin_label = @Translation("Crap Detect Message"),
 *   category = @Translation("Crap Detect"),
 * )
 */
class CrapDetectBlock extends BlockBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'msg_heading' => '',
      'msg_body' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['msg_heading'] = [
      '#title' => $this->t('Message heading'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 50,
      '#maxlength' => 255,
      '#default_value' => !empty($this->configuration['msg_heading']) ? $this->configuration['msg_heading'] : NULL,
    ];
    $form['msg_body'] = [
      '#title' => $this->t('Message body'),
      '#type' => 'textarea',
      '#default_value' => !empty($this->configuration['msg_body']) ? $this->configuration['msg_body'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['msg_heading'] = $form_state->getValue('msg_heading');
    $this->configuration['msg_body'] = $form_state->getValue('msg_body');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    return [
      '#theme' => 'crap_detect__message',
      '#heading' => $config['msg_heading'],
      '#body' => $config['msg_body'],
      '#attached' => ['library' => 'crap_detect/main'],
    ];
  }

}
