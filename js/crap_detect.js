// Crap Detect Script.
(function() {
  function displayMessage() {
    var msg = document.getElementById('crap-detect');
    if (!msg) {
      return false;
    }
    msg.style.display = 'block';
  }
  function checkBrowser() {
    // User-agent detection.
    var userAgent = window.navigator.userAgent;
    var isIe = (userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1);
    // Feature detection (flexbox).
    var isOutdated = document.body.style.flex === undefined || document.body.style.flexFlow === undefined;
    // Display message.
    if (isIe || isOutdated) {
      displayMessage();
    }
  }
  checkBrowser();
})();

