<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

 * Version
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

VERSION
-------

Version: 1.0.0-beta1


INTRODUCTION
------------

Old browsers will be detected in a simple way and a non-dismissable message is displayed on top of the page.


REQUIREMENTS
------------

- Drupal core 8 or 9
- JS enabled in browser


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

  - You can provide your own text for the message.
  - Multilingual support 

MAINTAINERS
-----------

Current maintainers:
 * Nikolay Grachev (Granik) - https://www.drupal.org/u/granik / developed by
 * Nico Grienauer (Grienauer) - https://www.drupal.org/u/grienauer


by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
